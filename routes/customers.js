const { Customer, validate } = require("../models/customer");
const auth = require("../middleware/auth");
// const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();

//GET for customers
router.get("/", auth, async (req, res) => {
  const customers = await Customer.find().sort("name");
  res.send(customers);
});

// /api/customers/1
router.get("/:id", auth, async (req, res) => {
  // Look up a customer in a MongoDB
  const customer = await Customer.findById(req.params.id);

  if (!customer) {
    return res
      .status(404)
      .send("The customer with the given ID was not found.");
  }
  res.send(customer);
});

// POST
router.post("/", auth, async (req, res) => {
  const { error } = validate(req.body); // equivelant to result.error
  if (error) {
    //400 Bad request
    return res.status(400).send(error.details[0].message);
  }
  const customer = new Customer({
    // id: customer.length + 1, // set by the database
    name: req.body.name,
    phone: req.body.phone,
    isGold: req.body.isGold,
  });
  await customer.save();
  res.send(customer);
});

//UPDATE
router.put("/:id", auth, async (req, res) => {
  // Validate
  // If invalid, return 400 - Bad request ---> copy lines from app.post
  // const result = validate(req.body);
  const { error } = validate(req.body); // equivelant to result.error
  if (error) {
    //400 Bad request
    return res.status(400).send(error.details[0].message);
  }

  //Lookup and update customer
  const customer = await Customer.findByIdAndUpdate(
    req.params.id,
    {
      name: req.body.name,
      isGold: req.body.isGold,
      phone: req.body.phone,
    },
    {
      new: true,
    }
  );
  // If not existing,  return 404 ---> copy lines from app.get
  if (!customer) {
    return res
      .status(404)
      .send("The customer with the given ID was not found.");
  }
  // Return the updated customer
  res.send(customer);
});

// Delete
router.delete("/:id", auth, async (req, res) => {
  // Look up the customer and delete it
  const customer = await Customer.findByIdAndRemove(req.params.id);

  // If not existing,  return 404 ---> copy lines from app.get
  if (!customer) {
    return res
      .status(404)
      .send("The customer with the given ID was not found.");
  }

  // Return the deleted customer
  res.send(customer);
});

module.exports = router;
