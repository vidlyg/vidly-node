// const asyncMiddleware = require('../middleware/async');
const validateObjectId = require("../middleware/validateObjectId");
const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const { Genre, validate } = require("../models/genre");
const mongoose = require("mongoose");
// const Joi = require('Joi');
const express = require("express");
const router = express.Router();

//GET for genres
router.get("/", async (req, res) => {
  // throw new Error('Could not get the genres.');
  const genres = await Genre.find().sort("name");
  res.send(genres);
});

// /api/genres/1
router.get("/:id", validateObjectId, async (req, res) => {
  // Check if id is valid

  // Look up a genre in a MongoDB
  const genre = await Genre.findById(req.params.id);

  // // Look up a genre in a MongoDB
  // const genre = genres.find(c => c.id === parseInt(req.params.id));
  if (!genre) {
    return res.status(404).send("The genre with the given ID was not found.");
  }
  res.send(genre);
});

// POST
router.post("/", auth, async (req, res) => {
  const { error } = validate(req.body); // equivelant to result.error
  if (error) {
    //400 Bad request
    return res.status(400).send(error.details[0].message);
  }
  const genre = new Genre({
    // id: genres.length + 1, // set by the database
    name: req.body.name,
  });
  await genre.save();
  res.send(genre);
});

// // UPDATE
// router.put('/:id', [auth, validateObjectId], async (req, res) => {
//   const { error } = validate(req.body);
//   if (error) return res.status(400).send(error.details[0].message);

//   const genre = await Genre.findByIdAndUpdate(req.params.id, { name: req.body.name }, {
//     new: true
//   });

//   if (!genre) return res.status(404).send('The genre with the given ID was not found.');

//   res.send(genre);
// });
router.put("/:id", [auth, validateObjectId], async (req, res) => {
  const { error } = validate(req.body);
  if (error) {
    //400 Bad request
    return res.status(400).send(error.details[0].message);
  }
  // if we lookup the genre in a mongodb database
  //Lookup and update genre
  const genre = await Genre.findByIdAndUpdate(
    req.params.id,
    { name: req.body.name },
    {
      new: true,
    }
  );
  // If not existing,  return 404 ---> copy lines from app.get
  if (!genre) {
    return res.status(404).send("The genre with the given ID was not found.");
  }

  // Return the updated genre
  res.send(genre);
});

// Delete
router.delete("/:id", [auth, admin, validateObjectId], async (req, res) => {
  // Look up the genre-item and delete it
  const genre = await Genre.findByIdAndRemove(req.params.id);

  // Look up the genre-item in a local array and delete it seperately
  // const genre = genres.find(c => c.id === parseInt(req.params.id));

  // If not existing,  return 404 ---> copy lines from app.get
  if (!genre) {
    return res.status(404).send("The genre with the given ID was not found.");
  }

  // // Delete if genre in a local array
  // const index = genres.indexOf(genre);
  // genres.splice(index, 1);

  // Return the deleted genre
  res.send(genre);
});

module.exports = router;
