const Joi = require("joi");
const validate = require("../middleware/validate");
const { Rental } = require("../models/rental");
const { Movie } = require("../models/movie");
const auth = require("../middleware/auth");
const express = require("express");
const router = express.Router();

// POST /api/returns {customerId, movieId}

router.post("/", [auth, validate(validateReturn)], async (req, res) => {
  // // Return 400 if customerId is not provided
  // if (!req.body.customerId) return res.status(400).send('customerId not provided');

  // // Return 400 if movieId is not provided
  // if (!req.body.movieId) return res.status(400).send('movieId not provided');

  // Return 404 if no rental is found for this customer/movie
  const rental = await Rental.lookup(req.body.customerId, req.body.movieId);

  if (!rental)
    return res.status(404).send("Rental not found for this customer/movie");

  // Return 400 if rental already processed
  if (rental.dateReturned)
    return res.status(400).send("Rental already processed");

  rental.return();

  await rental.save();
  // Increase the stock - of the movie returned.

  await Movie.update(
    { _id: rental.movie._id },
    {
      $inc: { numberInStock: 1 },
    }
  );

  // Return 200 if valid request
  return res.status(200).send(rental);
});

function validateReturn(req) {
  const schema = {
    customerId: Joi.objectId().required(),
    movieId: Joi.objectId().required(),
  };
  return Joi.validate(req, schema);
}

// Return the rental

module.exports = router;
