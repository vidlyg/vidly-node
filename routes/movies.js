const { Movie, validate } = require("../models/movie");
const { Genre } = require("../models/genre");
const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const validateObjectId = require("../middleware/validateObjectId");
const moment = require("moment");
const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();

//GET for movies
router.get("/", async (req, res) => {
  const movies = await Movie.find().sort("name");
  res.send(movies);
});

// /api/movies/1
router.get("/:id", validateObjectId, async (req, res) => {
  // Look up a movie in a MongoDB
  const movie = await Movie.findById(req.params.id);

  // // Look up a movie in a MongoDB
  // const movie = movies.find(c => c.id === parseInt(req.params.id));
  if (!movie) {
    return res.status(404).send("The movie with the given ID was not found.");
  }
  res.send(movie);
});

// POST
router.post("/", async (req, res) => {
  const { error } = validate(req.body); // equivelant to result.error
  if (error) {
    //400 Bad request
    return res.status(400).send(error.details[0].message);
  }

  const genre = await Genre.findById(req.body.genreId);
  if (!genre) return res.status(400).send("Invalid genre.");

  const movie = new Movie({
    title: req.body.title,
    genre: {
      _id: genre._id,
      name: genre.name,
    },
    numberInStock: req.body.numberInStock,
    dailyRentalRate: req.body.dailyRentalRate,
    publishDate: moment().toJSON(),
  });
  await movie.save();
  res.send(movie);
});

//UPDATE
router.put("/:id", [auth], async (req, res) => {
  // Validate
  // If invalid, return 400 - Bad request ---> copy lines from app.post
  // const result = validate(req.body);
  const { error } = validate(req.body); // equivelant to result.error
  if (error) {
    //400 Bad request
    return res.status(400).send(error.details[0].message);
  }

  const genre = await Genre.findById(req.body.genreId);
  if (!genre) return res.status(400).send("Invalid genre.");
  //
  const movie = await Movie.findByIdAndUpdate(
    req.params.id,
    {
      title: req.body.title,
      genre: {
        _id: genre._id,
        name: genre.name,
      },
      numberInStock: req.body.numberInStock,
      dailyRentalRate: req.body.dailyRentalRate,
    },
    { new: true }
  );
  // If not existing,  return 404 ---> copy lines from app.get
  if (!movie) {
    return res.status(404).send("The movie with the given ID was not found.");
  }

  // Return the updated genre
  res.send(movie);
});

// Delete
router.delete("/:id", [auth, admin], async (req, res) => {
  // Look up the movie-item and delete it
  const movie = await Movie.findByIdAndRemove(req.params.id);

  // Look up the movie-item in a local array and delete it seperately
  // const movie = movies.find(c => c.id === parseInt(req.params.id));

  // If not existing,  return 404 ---> copy lines from app.get
  if (!movie) {
    return res.status(404).send("The movie with the given ID was not found.");
  }

  // Return the deleted movie
  res.send(movie);
});

module.exports = router;
